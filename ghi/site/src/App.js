import React, { Fragment } from 'react';
import {gsap} from 'gsap';
import profile from './images/kim-profile.png';
import directrep from './images/direct-rep-thumbnail.png';
import recipes from './images/recipe-ideas.png';
import subaruservice from './images/SubaruService.png';
import newsfilter from './images/news-filter.png';
import guac from './images/guac-thumbnail.png';
import dinogame from './images/Dinogame-highscore.jpg';
import dinogame3D from './images/save-dino.png';
import duck from './images/duck-game.png';
import meteors from './images/meteor-game.png';
import resume from './images/resume-march.pdf';
import cyber from './images/cyber-range.pdf';
import veggierain from './images/veggie-rain.png';
import bootstrap from './logos/bootstrap.png';
import ccc from './logos/c++.png';
import css3 from './logos/css3.png';
import django from './logos/django.png';
import docker from './logos/docker.png';
import fastapi from './logos/fastapi-1.svg';
import gitlab from './logos/gitlab.png';
import godot from './logos/godot-logo.png';
import GSAP from './logos/GSAP.png';
import html5 from './logos/html5.png';
import js from './logos/javascript.png';
import mongo from './logos/mongodb.png';
import postgres from './logos/postgresql.png';
import python from './logos/python.png';
import react from './logos/react.png';
import render from './logos/render-1.png';
import './index.css';

function App() {

  const onEnterZoom = ({ currentTarget }) => {
    gsap.to(currentTarget, {scale: 1.2 });
  };

  const onLeaveZoom = ({ currentTarget }) => {
    gsap.to(currentTarget, {scale: 1 });
  };

  return (
    <Fragment>

<main>
  <section className="py-5 text-center container">
    <div className="row lg-5">
      <div className="col-lg-6 col-md-8 mx-auto">
      <img src={profile} className="rounded mx-auto d-block" alt="profile"/>
        <h1 className="fw-light">Kim Geraghty</h1>
        <h2 className="fw-light">Fullstack Software Engineer</h2>
        <p className="lead text-muted">Welcome to my personal site! I'm passionate about creating with code.</p>
        <div>
          <a href="mailto:kim.geraghty.kg@gmail.com" className="btn btn-primary my-2 ms-4" onMouseEnter={onEnterZoom} onMouseLeave={onLeaveZoom}>Contact me</a>

        </div>
        <div>
          <a href="https://www.linkedin.com/in/kim-geraghty/" className="btn btn-success my-2 ms-4" onMouseEnter={onEnterZoom} onMouseLeave={onLeaveZoom}>LinkedIn</a>
          <a href={resume} className="btn btn-success my-2 ms-4" onMouseEnter={onEnterZoom} onMouseLeave={onLeaveZoom}>Resume</a>
          <a href={cyber} className="btn btn-success my-2 ms-4" onMouseEnter={onEnterZoom} onMouseLeave={onLeaveZoom}>Cyber certificate</a>
        </div>
      </div>
    </div>
  </section>

  <div className="album py-5 bg-light">
    <div className="container">
    <h3 className="text-center py-2 m-2">Fullstack Projects</h3>
      <div className="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-3">
        <div className="col">
          <div className="card shadow-sm">
            <div className="card-body">
            <a href="https://kimgeraghty.gitlab.io/direct-rep/">
            <img className="bd-placeholder-img card-img-top" height="200" src={directrep} alt="direct-rep"/>
            </a>
              <p className="card-text">Using google civics and open AI to write letters to local representatives, Direct Rep allows users to participate more directly in democracy with minimum effort. Sign up for your own account today!</p>
              <div className="d-flex justify-content-between align-items-center">
                <div className="btn-group">
                  <button type="button" className="btn btn-sm btn-outline-secondary"><a href="https://kimgeraghty.gitlab.io/direct-rep/">Site</a></button>
                  <button type="button" className="btn btn-sm btn-outline-secondary"><a href="https://gitlab.com/kimgeraghty/direct-rep.git">Git</a></button>
                </div>
                <small className="text-muted">React + FastAPI</small>
              </div>
            </div>
          </div>
        </div>
        <div className="col">
          <div className="card shadow-sm">
            <div className="card-body">
            <a href="https://recipe-ideas-vptw.onrender.com">
            <img className="bd-placeholder-img card-img-top" height="200" src={recipes} alt="recipes"/>
            </a>
              <p className="card-text">Recipe Ideas is an application for finding recipes and deciding what to eat! It allows the user to sign up and save recipes suggested by a third party API. Thank you for your patience while the site loads.</p>
              <div className="d-flex justify-content-between align-items-center">
                <div className="btn-group">
                  <button type="button" className="btn btn-sm btn-outline-secondary"><a href="https://recipe-ideas-vptw.onrender.com">Site</a></button>
                  <button type="button" className="btn btn-sm btn-outline-secondary"><a href="https://gitlab.com/kimgeraghty/recipe_ideas">Git</a></button>
                </div>
                <small className="text-muted">Ruby on Rails</small>
              </div>
            </div>
          </div>
        </div>

        <div className="col">
          <div className="card shadow-sm">
            <div className="card-body">
            <a href="https://gitlab.com/kimgeraghty/subaru-service">
            <img className="bd-placeholder-img card-img-top" height="200" src={subaruservice} alt="subaru-service"/>
            </a>
              <p className="card-text">SubaruService (aka CarCar) is an application for managing an automobile dealership. It allows the user to enter and save data relevant to their inventory, their sales, and their service center.</p>
              <div className="d-flex justify-content-between align-items-center">
                <div className="btn-group">
                  <button type="button" className="btn btn-sm btn-outline-secondary"><a href="https://gitlab.com/kimgeraghty/subaru-service">Git</a></button>
                </div>
                <small className="text-muted">React + Django</small>
              </div>
            </div>
          </div>
        </div>
        <div className="col">
        <div className="card shadow-sm">
            <div className="card-body">
            <a href="https://gitlab.com/kim.geraghty.kg/news_filter">
            <img className="bd-placeholder-img card-img-top" height="200" src={newsfilter} alt="news-filter"/>
            </a>
              <p className="card-text">A Django application that uses a third-party API from newsapi to search for recent news articles by search word, or access them by search category. Shows the 3 most recent articles per query.</p>
              <div className="d-flex justify-content-between align-items-center">
                <div className="btn-group">
                  <button type="button" className="btn btn-sm btn-outline-secondary"><a href="https://gitlab.com/kim.geraghty.kg/news_filter">Git</a></button>
                </div>
                <small className="text-muted">Django + Newsapi</small>
              </div>
            </div>
          </div>
        </div>
        </div>
        </div>
        </div>
        <div className="album py-5 bg-light">
    <div className="container">
        <h3 className="text-center py-2 m-2">Games</h3>
      <div className="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-3">
        <div className="col">
          <div className="card shadow-sm">
            <div className="card-body">
            <a href="http://kimsgames.mygamesonline.org/home">
            <img className="bd-placeholder-img card-img-top" height="200" src={dinogame} alt="dino-game"/>
            </a>
              <p className="card-text">In-browser game in Javascript using the canvas element. The arrow keys enable keyboard-based game play.
There are 4 levels with increased speed and randomization of movement changes. Save the dino!
</p>
              <div className="d-flex justify-content-between align-items-center">
                <div className="btn-group">
                  <button type="button" className="btn btn-sm btn-outline-secondary"><a href="http://kimsgames.mygamesonline.org/home">Site</a></button>
                  <button type="button" className="btn btn-sm btn-outline-secondary"><a href="https://gitlab.com/kim.geraghty.kg/dino-game.git">Git</a></button>
                </div>
                <small className="text-muted">Javascript</small>
              </div>
            </div>
          </div>
        </div>
<div className="col">
          <div className="card shadow-sm">
            <div className="card-body">
            <a href="https://kimgeraghty.itch.io/save-dino">
            <img className="bd-placeholder-img card-img-top" height="200" src={dinogame3D} alt="dino-game-3D"/>
            </a>
              <p className="card-text">Created with Godot game engine. Includes UI features with pause and resume game functions, life gauge and score tracker. Move the camera with the mouse and the player with W/A/S/D.
</p>
              <div className="d-flex justify-content-between align-items-center">
                <div className="btn-group">
                  <button type="button" className="btn btn-sm btn-outline-secondary"><a href="https://kimgeraghty.itch.io/save-dino">Site</a></button>
                  <button type="button" className="btn btn-sm btn-outline-secondary"><a href="https://www.linkedin.com/feed/update/urn:li:activity:7037231638711320576/">Preview</a></button>
                </div>
                <small className="text-muted">Godot | GD Script | Blender</small>
              </div>
            </div>
          </div>
        </div>
        <div className="col">
          <div className="card shadow-sm">
            <div className="card-body">
            <a href="https://kimgeraghty.itch.io/veggie-rain">
            <img className="bd-placeholder-img card-img-top" height="200" src={veggierain} alt="veggie-rain"/>
            </a>
              <p className="card-text">Created with Godot, using 3D animation and GD Script. It's raining veggies! You can move the camera view with the mouse and the player with your keyboard.
</p>
              <div className="d-flex justify-content-between align-items-center">
                <div className="btn-group">
                  <button type="button" className="btn btn-sm btn-outline-secondary"><a href="https://kimgeraghty.itch.io/veggie-rain">Site</a></button>
                </div>
                <small className="text-muted">Godot | GD Script</small>
              </div>
            </div>
          </div>
        </div>
        <div className="col">
          <div className="card shadow-sm">
            <div className="card-body">
            <a href="https://kimsgames.netlify.app/">
            <img className="bd-placeholder-img card-img-top" height="200" src={guac} alt="guac"/>
            </a>
              <p className="card-text">In-browser game created with React, using GSAP animation and function-based components complete with a timer and score count. Whac some avocados to make the guacamole!</p>
              <div className="d-flex justify-content-between align-items-center">
                <div className="btn-group">
                  <button type="button" className="btn btn-sm btn-outline-secondary"><a href="https://kimsgames.netlify.app/">Site</a></button>
                  <button type="button" className="btn btn-sm btn-outline-secondary"><a href="https://gitlab.com/kim.geraghty.kg/guacamole.git">Git</a></button>
                </div>
                <small className="text-muted">React</small>
              </div>
            </div>
            </div>
            </div>
            <div className="col">
          <div className="card shadow-sm">
            <div className="card-body">
            <a href="https://kimgeraghty.itch.io/ducks/">
            <img className="bd-placeholder-img card-img-top" height="200" src={duck} alt="duck"/>
            </a>
              <p className="card-text">Created with Python and pygame, deployed with pygbag. Thank you for your patience when loading the game. Hit the ducks as fast as you can by clicking on the mouse!</p>
              <div className="d-flex justify-content-between align-items-center">
                <div className="btn-group">
                  <button type="button" className="btn btn-sm btn-outline-secondary"><a href="https://kimgeraghty.itch.io/ducks/">Site</a></button>
                  <button type="button" className="btn btn-sm btn-outline-secondary"><a href="https://gitlab.com/kimgeraghty/kim-pygames.git">Git</a></button>
                </div>
                <small className="text-muted">Python</small>
              </div>
            </div>
            </div>
            </div>
            <div className="col">
          <div className="card shadow-sm">
            <div className="card-body">
            <a href="https://kimgeraghty.itch.io/meteors/">
            <img className="bd-placeholder-img card-img-top" height="200" src={meteors} alt="meteors"/>
            </a>
              <p className="card-text">Created with Python and pygame, deployed with pygbag. Thank you for your patience when loading the game. Move the ship with the mouse to avoid the meteors, and click to shoot them!</p>
              <div className="d-flex justify-content-between align-items-center">
                <div className="btn-group">
                  <button type="button" className="btn btn-sm btn-outline-secondary"><a href="https://kimgeraghty.itch.io/meteors/">Site</a></button>
                  <button type="button" className="btn btn-sm btn-outline-secondary"><a href="https://gitlab.com/kimgeraghty/kim-pygames.git">Git</a></button>
                </div>
                <small className="text-muted">Python</small>
              </div>
            </div>
            </div>
            </div>
      </div>
    </div>
  </div>

  <div className="album py-5 bg-light">
    <div className="container">
      <div className="row row-cols-1 g-3 mx-auto">
        <h3 className="text-center py-2 m-2">Programming skills</h3>
          <div className="card shadow-sm flex-row flex-wrap py-2 justify-content-evenly">
            <img className="rounded float-start m-1" width="100" height="100" src={css3} alt="css3"/>
            <img className="rounded float-start m-1" width="100" height="100" src={ccc} alt="c++"/>
            <img className="rounded float-start m-1" width="100" height="100" src={html5} alt="html5"/>
            <img className="rounded float-start m-1" width="100" height="100" src={js} alt="js"/>
            <img className="rounded float-start m-1" width="100" height="100" src={python} alt="python"/>
            <img className="rounded float-start m-1" width="100" height="100" src={django} alt="django"/>
            <img className="rounded float-start m-1" width="100" height="100" src={docker} alt="docker"/>
            <img className="rounded float-start m-1" width="100" height="100" src={fastapi} alt="fastapi"/>
            <img className="rounded float-start m-1" width="100" height="100" src={gitlab} alt="gitlab"/>
            <img className="rounded float-start m-1" width="100" height="100" src={godot} alt="godot"/>
            <img className="rounded float-start m-1" width="100px" height="100" src={bootstrap} alt="bootstrap"/>
            <img className="rounded float-start m-1" width="100" height="100" src={GSAP} alt="GSAP"/>
            <img className="rounded float-start m-1" width="100" height="100" src={mongo} alt="mongo"/>
            <img className="rounded float-start m-1" width="100" height="100" src={postgres} alt="postgres"/>
            <img className="rounded float-start m-1" width="100" height="100" src={react} alt="react"/>
            <img className="rounded float-start m-1" width="100" height="100" src={render} alt="render"/>
        </div>
      </div>
    </div>
  </div>
</main>

<footer className="text-muted py-5">
  <div className="container">
    <p className="float-end mb-1">
      {/* <Link href="#">Back to top</Link> */}
    </p>
    <p className="mb-1 text-center">Thank you for your visit!</p>
  </div>
</footer>
</Fragment>
  );
}

export default App;
